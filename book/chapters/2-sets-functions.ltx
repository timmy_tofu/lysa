\ch{Booleans, simple logic, and simple operators}

Before we get into interesting content, you have to understand some
notation. This stuff is pretty easy. This will likely be the shortest and
easiest chapter in the book.

\begin{itemize}
\item $A \implies B$ means that ``if $A$ is \truenm, that means that $B$ must be
  \truenm.'' Note that this \xtb{does not} mean that ``if $B$ is \truenm, then
  $A$ is \truenm.'' Always follow the arrow. $A \implies B$ should be read as
  ``$A$ implies $B$.''

\item $A \impliedby B$ is the same thing as writing $B \impliedby A$. It's often
  convenient to write $A \impliedby B$ though. $A \impliedby B$ should be read
  ``$A$ is implied by $B$.''

\item $A \iff B$ means that $A \implies B$ and $B \implies A$. You can think of
  $A \iff B$ as meaning ``Saying $A$ is the same thing as saying $B$.''
  
\item $A \land B$ should be read as ``$A$ logical-and $B$.'' Both have to be
  \truenm. If one of them is \falsenm, than $A \land B$ is \falsenm. Likewise,
  if $A$ and $B$ are both \truenm, then $A \land B$ is \truenm.

\item $A \lor B$ should be read as ``$A$ logical-or $B$.'' With $A \lor B$, if
  one of $A$ and $B$ is \truenm, then $A \lor B$ is \truenm.
\end{itemize}

These values - true or false, they come in handy. That is, we can treat them
like numbers or letters. They have mildly interesting properties on their own.

\ss{Idris}

This section provides a ``working example'' of the above in Idris. If you don't
know what that is, you are a bad person. Go back and read \cref{intro-idris}!
% 
% Edit: Yes you do.
% 
% Strictly speaking, you don't \xti{have} to read this subsection, but I think it
% would be beneficial if you do.

Open up an Idris prompt, and enter \code{:type True}. That is basically asking
Idris ``what type of thing is \truenm?'' Idris will tell you. Also do the same
thing for \falsenm. Here's what happens when I do it:

\begin{lstlisting}
Idris> :type True
Prelude.Bool.True : Bool
Idris> :type False
Prelude.Bool.False : Bool
\end{lstlisting}

If you ask Idris what the type of \code{True} or \code{False} is, it will tell
you that they are \code{Bool}s.\footnote{For the most part, you can ignore all
  the weird stuff on the left-hand-side, for the time being. For instance, when
  I ran \code{:type True}, Idris switched \code{True} to
  \code{Prelude.Bool.True}. These are odd caveats of Idris's syntax, which I
  don't have time to explain right now. We'll get to them later, I promise.}
You're probably thinking ``what the hell is a \code{Bool}?'' \code{Moreover, why
  the hell is this guy printing all this stuff in monospace}? Well, explaining
this sorta stuff, that's what I'm here for. \code{Bool} is short for Boolean,
which means ``\truenm or \falsenm.''  They're named after a mathematician named
George Boole, who studied the algebraic manipulation of \truenm and \falsenm.

The reason I'm printing stuff \code{in monospace} is to say ``hey, this is
code.'' More importantly, printing stuff in monospace eschews some formatting
flukes caused by variable-width text. In normal paragraph text, these flukes are
fine --- they are even helpful --- but they cause some ambiguities in code. For
that reason, the standard thing to do is to write code in monospace.

In Idris, and in most programming languages, the $\land$ operator is replaced
with \code{\&\&}. We know that, in Idris \truenm and \falsenm are both Boolean
values. What about $\true \land \false$?

\begin{lstlisting}
Idris> :type (True && False)
True && Delay False : Bool
\end{lstlisting}

\xti{So, wait, \truenm and \falsenm are both Boolean values, but
  $\true \land \false$ is also a Boolean value?}

Yes, Timmy, now try to keep up.

Now, from the explanation of $\land$ above, $\true \land \false$ should only be
true if both \truenm and \falsenm are \truenm. Well, that's obviously not the case,
so $\true \land \false$ should turn out to be \falsenm, right? Let's see!

\begin{lstlisting}
Idris> True && False
False : Bool
\end{lstlisting}

Yay! You figured out some stuff! This dealingswith of \truenm and \falsenm is
called ``Boolean algebra.'' Boolean algebra deals only with two values, \truenm
and \falsenm, so it's understandably one of the simpler algebras. Anyway, more
Boolean algebra to follow:

We've discussed that $\true \iff \true$, and $\false \iff \false$. We've also
shown how $\true \land \false \iff \false$. What about $\true \lor \false$?

Remember, $\true \lor \false$ is \truenm if one of them is \truenm. Both of them
can be \truenm; only one of them has to be. You \xti{do} remember, right? Since
one of them is \truenm, $\true \lor \false$ should therefore be $\true$.

In Idris --- and most programming languages --- $\lor$ is replaced with
\code{||}.

\begin{lstlisting}
Idris> True || False
True : Bool
\end{lstlisting}

Alright, so we've taken a cursory look at \truenm and \falsenm. I'm going to
leave some problems as exercises to the reader (that's you). Don't worry too
much if you don't get them, the solutions are on the next page!

\begin{ExcList}
  \Exercise{Is it true that, for all $A$ and $B$, $A \land B \iff B \land A$?
    Why or why not?\footnote{Hint: You might want to try this out in Idris. Note
      that, in this book, ``Idris said so'' is not a sufficient means of
      proof. Well, that's not quite true. In this chapter at least, ``Idris said
      so'' isn't sufficient proof.}}

  \Exercise{Is it true that, for all $A$ and $B$, $A \lor B \iff B \lor A$? Why
    or why not?}
\end{ExcList}


% TODO:
% * Explain more stuff about \lor and \land
% * Explain the first few peano axioms (about equality)
% * Explain the transition of logic.
% * Exercises

\ch{Sets, Proofs, and Functions}

Anyway, now that that boring introduction is out of the way, we can get to some
math! The first thing we are going to cover are \xti{set}s.

Sets were first studied by Georg Cantor, a German mathematician, in the second
half of the nineteenth century.  Back in his own day, the results Cantor found
by studying sets were considered so thoroughly bizarre that many of his
colleagues simply refused to believe that Cantor could be right.  In the end,
Cantor turned out to be right all along. His ideas can be found in any
introductory text on mathematics---including this one.

Sets are basically like lists--- think ``your grocery list'' or ``your
to-do-list'' --- except there's no duplication, and there's no intrinsic
order. Examples:

\begin{enumerate}
\item $\mset{0, 1, 2, 3}$ is a set.
\item $\mset{ 1, 2, 0, 3}$ is the same set, because the order is of no
  consequence.
\item $\mset{ 1, 1, 2, 0, 3}$ is the same set, because the duplication can be
  ignored.
\item $\mset{ 8, 1, 2, 0, 3}$ is a different set, because there's a new element.
\end{enumerate}

\s{Elements, Subsets, and Supersets}

\begin{enumerate}
\item $0,1,2,3$ {\bf is not} a set, it is instead the numbers $0$ through $3$, which
  are to be considered separately.
\item $\mset{0,1,2,3}$ {\bf is} indeed a set (notice the braces).
\item If some object is in a set, the notation is
  `$\mathrm{OBJECT} \in \mathrm{SET}$', which should be read ``OBJECT is an
  element of SET''.
\item If the object is not in the set, the notation is
  `$\mathrm{OBJECT} \not \in \mathrm{SET}$', which should be read ``OBJECT is
  not an element of SET''.
\item $0 \in \mset{0,1,2,3}$ should be read ``$0$ is an element of
  $\mset{0,1,2,3}$'', and indeed it is.
\item $0,1 \in \mset{0,1,2,3}$ should be read ``$0$ and $1$ {\bf are both} elements
  of $\mset{0,1,2,3}$''. They are.
\item $\mset{0,1} \not \in \mset{0,1,2,3}$ should be read ``$\mset{0, 1}$ {\bf
    is not} an element of $\mset{0,1,2,3}$''. $\mset{0,1}$ is indeed not an
  element of $\mset{0,1,2,3}$.
\end{enumerate}

Now, here, we're faced with an interesting problem. $\mset{0,1,2,3}$
encapsulates $\mset{0,1}$, but $\mset{0,1}$ is not an element of
$\mset{0,1,2,3}$. So, how do we express this notion of `encapsulation'? The
answer is by using ``subset'' notation.

\newcommand{\zo}{\mset{0,1}}
\newcommand{\zf}{\mset{0,1,2,3}}
\newcommand{\zzf}{$\zo \subset \zf$}
\newcommand{\zzef}{$\zo \subseteq \zf$}

\begin{enumerate}
\item \zzef is {\bf true}. \zzef should be read as ``$\zo$ is an {\bf improper}
  subset of $\zf$.''
\item \zzf is {\bf true}. \zzf should be read as ``$\zo$ is a {\bf proper}
  subset of $\zf$.''
\item $\zf \subeq \zf$ is {\bf true}. Intuitively, you should think ``$\zf$ is
  entirely encapsulated inside of $\zf$.''
\item $\zf \subof \zf$ is {\bf false}. This highlights the difference between
  \subofnm and \subeqnm.

  \zzf implies that $\zf$ contains some stuff that \b{is not} contained in
  $\zo$. \zzef does not have this implication. That is, \zzef does not say for
  sure that $\zf$ has more stuff than $\zo$, it just acknowledges it as a
  possibility.
\end{enumerate}


Alright, my apologies, that was a lot of stuff to throw at you at once, and you
probably remember absolutely none of it. However, you'll get used to that
particular \xti{set} of stuff\footnote{I, for one, would never condone such
  terrible puns.} as time goes along.

\ss{Formal Definitions}

I'm all for formalism up to the point at which it hinders your understanding of
the material. In most cases, formalism does hinder your understanding. However,
in this case --- when talking about definitions --- precise and formal
definitions are usually the most helpful.

\begin{description}
\item[Set] A collection of items, called ``elements''. The elements have no
  intrinsic order, and no multiplicity.
\item[Elements] Given a set $S$ and an element $x$, $x \in S$ is the formal
  notation for saying ``$S$ contains $x$.''
\item[Non-elements] Given a set $S$ and an element $x$, $x \not\in S$ is the
  formal notation for saying ``$S$ does not contain $x$.''\footnote{In general,
    if you want to say ``something is not true,'' you cross out the operator in
    question. For instance, if you want to say ``$x=y$ is not true'', the
    notation is $x \ne y$.}
\item[Questions] If you want to pose the question ``does $S$ contain $x$?'', the
  notation is $x \Qin S$.\footnote{In general, if you want to pose a question,
    you put a $?$ over the operator in question.}
\item[Improper subsets] If you want to say ``all of the elements in $S$ are in
  $T$, but not necessarily the other way around'' the notation is $S \subeq T$.
  This is to be read ``$S$ is an improper subset of $T$''.

  More formally, $S \subeq T$ means that for all elements $x$, such that
  $x \in S$, it is true that $x \in T$. In purely mathematical notation, this is
  written

  \[ S \subeq T \iff \forall \mset{x \mid x \in S} \comma x \in T\]
  
  That should be read ``$S$ is an improper subset of $T$ if and only if for all
  $x$, such that $x$ is an element of $S$, $x$ is an element of $T$.''

\item[Improper supersets] If you want to say ``$S$ contains all of the elements
  in $T$'', you write $S \supeq T$. $S \supeq T$ should be read ``$S$ is an
  improper superset of $T$.'' Note that $S \supeq T$ is the same as writing
  $T \subeq S$, but it's often convenient to have the superset notation.

\item[Equality of Sets] A set $S$ is equal to another set $T$ if and only if
  $S \subeq T$ and $S \supeq T$. The notation for this is $S = T$, which is to
  be read ``$S$ is equal to $T$.'' That is,

  \[ S = T \iff \parens{\forall \mset{x \mid x \in S} \comma x \in T}
  \land \parens{\forall \mset{x \mid x \in T} \comma x \in S} \]

\item[Proper subsets] $S$ is a proper subset of $T$ if and only if $S \subeq T$
  and $S \ne T$. This is to be written $S \subof T$
  
\item[Proper supersets] $S$ is a proper superset of $T$ if and only if
  $S \supeq T$, and $S \ne T$ This is to be written $S \supof T$.
\end{description}

\ss{Exercises}

The next page has some exercises.  I would recommend that you do all of the
exercises. Many books berate you with dozens of exercises all exercising the
same problem. I'm too lazy to do that; instead, I'm going to give you a small
number of exercises. The reason I do that is, I have to solve the exercises,
too. The solutions are on the page after the exercises.

\begin{ExcList}
  \Exercise { Does it make sense to ask ``how many times does $S$ contain $n$?''
    Why or why not?}  \Answer {No, because repetition doesn't matter.}

  \Exercise {
    \begin{enumerate}
    \item If $S$ is a proper superset of $T$, is it also true that
      $S \nsubseteq T$?  In math notation, 
      
      \[ S \supof T \Qimplies S \nsubseteq T \]
    \item What about the opposite? That is, if $S \nsubseteq T$, is it also true
      that $S \supof T$? That is,

      \[ S \supof T \Qimpliedby S \nsubseteq T \]
    
    \end{enumerate}

    Explain why or why not. }

  \Exercise {
    \begin{enumerate}
    \item If $S \subof T$, is it the case that there are elements of $T$ which
      are not in $S$? Why or why not? To put this in mathematical notation:
    
      \[ S \subof T \Qimplies \exists \mset{x \mid x \in T \land x \not\in S}\]

      The expression above should be read ``$S$ is subset of $T$, question
      implies there exists a set of $x$, such that $x$ is an element of $T$, and
      $x$ is not an element of $S$''
      
    \item What about the opposite, that is

      \[ S \subof T \Qimpliedby \exists \mset{x \mid x \in T \land x \not\in S}\]
    \end{enumerate}
  }
  

\Exercise {If you know $S \subof T$, is it also true that $S \subeq T$? In math
  notation:

  \[ S \subof T \Qimplies S \supeq T\]

  Why or why not?}

\Answer {Yes. $S \subeq T$ means that one of $\mset{S \subof T \comma S = T}$ is
  true. If $S \subof T$ is true, then the condition of $S \subeq T$ is
  satisfied. Thus, $S \subof T \implies S \subeq T$.}


\Exercise {What about, if you know $S = T$, is it also true that $S \subeq T$?
  Why or why not? In math notation,

  \[S = T \Qimplies S \subeq T\] }

\Answer {Yes, by the same logic above. If $S = T$, then the conditions for
  $S \subeq T$ are satisfied.}

\Exercise {If it is known that $S \subeq T$, is it also true that $S \subof T$?
  Why or why not?
    
  \[ S \subeq T \Qimplies S \subof T\] }

\Answer {No.

  \begin{itemize}
  \item $S \subof T$ can be stated as ``all of $\mset{S \subeq T, S \not = T}$
    are true.''
  \item By the previous problem, if $S \not = T$ is false (i.e.  $S = T$ is
    true), then $S \subeq T$ is true.
  \item In this case, the set above could be written as
    $\mset{S \subeq T, S \not = T} = \mset{True, False}$.  $S \subof T$ means
    that both of those have to be true. That is, $S \subof T$ is true if (and
    only if) $\mset{S \subeq T, S \not = T} = \mset{True, True}$.
  \item In this case, we have
    $\mset{S \subeq T, S \not = T} = \mset{True, False}$, which means
    $S \not \subof T$.
  \item Thus, we have just found a case in which $S \subeq T$ is true, but
    $S \subof T$ is false. Therefore, it cannot be the case that
    $S \subeq T \implies S \subof T$. Or, to put it another way,
    $S \subeq T \notimplies S \subof T$.
  \end{itemize}}
  
\Exercise { Let's look back at $S$, $T$, and $n$. Let's say that you know
  $n \in S$ (that is, $n$ is in $S$). You also know that $S \subeq T$. Does this
  also mean $n \in T$? Why or why not? To put this in mathematical notation:

  \[ \mset{n \in S\comma S \subeq T} \Qimplies n \in T \]
} \Answer { Yes. $S \subeq T$ means that all of the elements in $S$ are also in
  $T$, but not necessarily the other way around.. Thus, if $n \in S$, it must be
  true that $n \in T$.  }
    

\Exercise {Since I'm lazy, and the math notation is probably much easier for you
  to understand,

  \[ \mset{n \in T \comma S \subeq T} \Qimplies n \in S \]}
\Answer {No. $S \subeq T$ means that it is possible that $S \subof T$ is
  true. $S \subof T$ means that $T$ has all of the elements in $S$, but there
  are also more elements. It's entirely plausible that $n$ is one of the
  additional elements. Thus,

  \[ \mset{n \in T \comma S \subeq T} \notimplies n \in S \]}

\Exercise {What about the opposite? That is,

  \[ S \subeq T \Qimpliedby \mset{n \in S, n \in T} \]}
\Answer{No. $n \in S$ can be stated as $\mset{n} \subeq S$. Likewise,
  $n \in T \iff \mset{n} \subeq T$. This means there are 3 possible cases for
  the relation between $S$ and $T$:

  \begin{enumerate}
  \item $S = T$
  \item $S \subof T$
  \item $S \supof T$
  \end{enumerate}
    
  $S \subeq T$ means that either 1 or 2 is true, but the third one is false. The
  third one can be true when $\mset{n \in S, n \in T}$ is true. Thus, we have
  found a case in which $\mset{n \in S, n \in T}$ is true, but $S \subeq T$ is
  false. Thus, it must be that.
    
    \[ S \subeq T \notimpliedby \mset{n \in S, n \in T} \]}
\end{ExcList}


\s{Numeric sets}

Now, I'm going to throw more stuff at you. I'm sorry for all the notation, but
it is important that you get used to it. There are a bunch of common sets you'll
encounter in the real world.

% Again, a list is easier
\begin{enumerate}
\item First of all, there are the integers: these are just the whole numbers,
  like $-3, -2, -1, 0, 1, 2$.  Traditionally, this set is denoted $\mathbb{Z}$.

\item The natural numbers, denoted $\mathbb{N}$, are all non-negative
  integers\footnote{Conventions differ as to what the natural numbers are.
    Everyone agrees that positive whole numbers (1, 2, 3\ldots) are natural.
    Some people say $0$ is also a natural numbers, while others disagree.  In
    this book, 0 {\it is} a natural number, so the natural numbers are 0, 1, 2,
    and so on. This might seem like a stupid fight, why can't we just choose a
    side? However, it actually turns out to be really important.}.

\item The set of ``rational'' numbers, denoted $\mathbb{Q}$ is the set of all
  numbers that can be written as a quotient (or fraction) $\frac{p}{q}$, where
  $p$ and $q$ are integers.

  In mathematical notation, you would write this as
  $\left\{ \frac{p}{q} \mid p,\,q \in \mathbb{Z} \right\}$.  You should read
  that as ``the set of all numbers $p$ over $q$, such that $p$ and $q$ are
  elements of $\Z$.''

\item Finally, the real numbers, denoted $\mathbb{R}$, contain everything on the
  number line.  Equivalently, a real number is just any number you can write
  down by writing down an integer, a decimal point, and then writing any
  sequence of digits after the decimal point (even an infinite sequence).

  Let us show you an example. Let's write down an integer. $2$. There we
  go. Now, let's just write down a bunch of digits all in a row. $215455211$. By
  the definition in the previous paragraph, $2.215455211$ is a real number. You
  should get in the habit of writing ``$2.215455211$ is a real number'' as
  $2.215455211 \in \R$.
\end{enumerate}

Sets certainly look simple enough to understand intuitively, but are they also
interesting enough to be worth studying?  The answer to that is a resounding
`yes.' Exciting, right?

To give you a taste of the results, let's take a look at some of them.

Intuitively, sets can be of different sizes: it's clear that the empty set is
the smallest possible set, and that $\{2, 4\}$ is smaller than $\{1, 2, 3\}$.
When we get to infinite sets, however, the intuition fails.

Cantor formalised this intuition, and the formal version also worked for
infinite sets.  The results he got were very surprising: for example, he found
that there are as many natural numbers as rational numbers, but that there are
strictly more real numbers.  Even worse, he proved that given any set, there is
always a set larger than it.  If you've ever heard of people talking about
``different kinds of infinity'', this is probably what they meant.

\sss{Exercises}

\begin{enumerate}
  \item Suppose $x$ is some object and $S$ is a set.  Does it make sense to ask
    ``How many times does $S$ contain $x$?''
  \item Earlier, we constructed the set $\{3, 5\}$.  We can also construct the
    set $\{5, 3\}$.  Are these the same set?
  \item Are the following true or false?
    \begin{itemize}
      \item $\emptyset \in \emptyset$
      \item $\emptyset \in \{\emptyset\}$
      \item $\emptyset = \{\emptyset\}$
      \item $\{\emptyset\} \in \{\emptyset\}$
    \end{itemize}
\end{enumerate}

\s{Functions}

% So, we're sort of faced with a problem.

% I'm going to try to explain functions in Layman's terms

% \xti{Function} is like \xti{set}, in that it's a semi-common term, which us math
% people use in a slightly different way. You can think of a ``function'' like a
% magic box. You put some value into the function, and then a different value
% comes out. Moreover, if you input the same thing multiple times, you get the
% same output. This has the consequence that, for every input, there is at most 1
% output value. That is, you can't have a function t

% \ss{More formally}

A function from a set $X$ to a set $Y$ specifies for every element of $X$ a
corresponding element of $Y$.  In other words, if $f$ is a function from $X$ to
$Y$ (denoted $f : X \to Y$) and $x \in X$, then $f(x) \in Y$.  A lot of high
school mathematics is about functions from $\mathbb{R}$ to $\mathbb{R}$, even if
they are never presented that way.  For example, solving linear equations is
actually a question about functions: ``given a function
$f : \mathbb{R} \to \mathbb{R}$ defined by

\[
  f(x) = 5x - 10
\]

find all $r \in \mathbb{R}$ such that $f(r) = 0$.''

It turns out that by taking sets $X$ and $Y$ and asking what kind of functions
exist between them, we can find out a lot about $X$ and $Y$ themselves.  We'll
continue this line of thought after some exercises.

\newpage

\sss{Exercises}

\begin{enumerate}
  \item We can construct the set $\{0, 1\}$.  What functions can you find from
    $\{0, 1\}$ to $\{0, 1\}$?  How many such functions are there?
  \item Can you find a function from $\emptyset$ to $\{0, 1\}$?  What about in
    the other direction?
  \item Suppose $X$ is a set.  Prove that there exists at least one function
    from $X$ to $X$.  How can this function be defined?  Why does this work if
    $X$ is the empty set?
  \item Suppose $X$, $Y$, and $Z$ are sets, and you are given functions $f : X
  \to Y$ and $g : Y \to Z$.  Can you make a function from $X$ to $Z$?  How?
  \item Suppose $X$ and $Y$ are sets, $f : X \to Y$, and $x, y \in X$.  Do you
    think $x = y$ implies $f(x) = f(y)$?  Why, or why not?
\end{enumerate}

\newpage

\sss{Solutions}

% Left as an exercise...

\newpage

\s{Injective Functions}

In the exercises, you saw that if $f : X \to Y$ and you had $x, y \in X$ such
that $x = y$, then necessarily $f(x) = f(y)$.  A function $f$ is said to be
injective if the converse is also true: if $x, y \in X$ and $x \neq y$, then
$f(x) \neq f(y)$.

Let's consider some examples.  Let $f : \mathbb{N} \to \mathbb{Z}$ be defined by
$f(x) = x$.  Is this definition correct?  Well, we need to check that for any $n
\in \mathbb{N}$, we have $f(n) \in \mathbb{Z}$.  But by definition, $f(n) = n$,
so we need to check that if $n \in \mathbb{N}$, then also $n \in \mathbb{Z}$.
Every non-negative integer is certainly an integer, so yes, the definition is
correct.

Now, is this function injective?  For that, let $n, k \in \mathbb{N}$ so that
$f(n) = f(k)$.  We need to prove that $n = k$.  Again, this is easy: $f(n) = n$
and $f(k) = k$, so by putting the three equalities together, we have $n = k$.

We can construct the set $\{f(0), f(1), f(2)\ldots\}$.  If we want to be
precise, we can use the {\it set comprehension} notation:

\[
  \{ f(x) \, | \, x \in \mathbb{N} \}
\]

You should read this as ``the set that contains $f(x)$ for each $x \in
\mathbb{N}.$''  That's rather a mouthful, so we'll abbreviate this by
$f(\mathbb{N})$.  Just imagine that instead of applying $f$ to one value in
$\mathbb{N}$, you're applying it to all the values and looking at the results as
a set.

We can now ask ourselves: how big is $f(\mathbb{N})$?  In this case, we can see
that $\mathbb{N} = f(\mathbb{N})$, so of course they're exactly the same size.
However, what if instead of the $f$ we chose, we had chosen $f(x) = -x$, or $g :
\mathbb{N} \to \mathbb{Q}$ defined by $g(x) = \frac{1}{1+x}$?  Prove that both
of these are injective functions, and imagine $f(\mathbb{N})$ and
$g(\mathbb{N})$.  How big are these sets?

It should be clear that given any sets $X$ and $Y$ and any function $f : X \to
Y$, $f(X)$ cannot possibly be bigger than $X$.  For every element $y \in f(X)$,
there is some element $x \in X$ so that $f(x) = y$; that's how we defined $f(X)$
in the first place.

% TODO: rewrite this to require less proof.
On the other hand, if $f$ is injective, $f(X)$ also shouldn't be any smaller
than $X$.  Why?  Well, we can just define a function the other way.  Let $g :
f(X) \to X$, and define $g(f(x)) = x$.  Now we know that $g(f(X))$ is no larger
than $f(X)$.  But $g(f(X)) = \{ g(f(x)) \, | \, x \in X \} = \{ x \, | \, x \in
X \} = X$!  That means, by the argument above, that $f(X)$ is no larger than
$X$, just as we wanted.

The careful reader might notice that the definition of $g$ is fishy.  Does a
function defined this way really work?  The answer is that it does, but only
because we were careful about $f$.  We know that if we have an element $y \in
f(X)$, then there is an element $x \in X$ so that $y = f(x)$.  This means that
$g$ really is defined for all $y \in f(X)$.  That's not all, though.
Additionally, suppose there was also a $z \in X$ so that $y = f(z)$.  Then $g(y)
= g(f(x)) = x$, but also $g(y) = g(f(z)) = z$.  For $g$ to be a function, we
need to prove that $x = z$.  Here is where we need the injectivity of $f$: we
know that since $f(x) = y = f(z)$, that $f(x) = f(z)$, and thus $x = z$.
Therefore, $g$ really is a function, and so our argument holds.

Our intuition thus tells us that however we define size, we should make sure
that for any set $X$ and any injective function $f : X \to Y$, we should have
$X$ and $f(X)$ be of equal size.  As $f(X)$ is just a part of $Y$, this means
that $X$ should also be no bigger than $Y$.  By themselves, all these loose bits
of intuition might not seem very useful, but in the next section we'll use them
to show that a certain approach to define size {\it doesn't} work.  First,
however, some exercises.

\sss{Exercises}

\begin{enumerate}
  \item In an older exercise, you saw there were four functions from $\{0, 1\}$
    to itself.  How many injective functions are there?  What about between
    $\{0, 1, 2\}$ and itself?  What if you have even more elements?
  % Something about seeing an injective function as an embedding.
\end{enumerate}

\s{Subsets}

When people first hear that there are as many integers as natural numbers, their
reaction is often ``Surely that can't be right?  Every natural number is an
integer, and there are some others, too!''

To make this argument formal we should introduce the notion of a subset.  We say
that $X$ is a subset of $Y$ if every element of $X$ is also an element of $Y$.
We denote this by $X \subseteq Y$.  We have, then $\emptyset \subseteq \mathbb{N}
\subseteq \mathbb{Z} \subseteq \mathbb{Q} \subseteq \mathbb{R}$.  We will use $X
\subset Y$ when $X$ is a subset of $Y$, but $X$ and $Y$ are not equal.

Now let's denote the size of $X$ by $|X|$.  Can we define this size in such a
way that for finite sets, $|X|$ is the number of elements in $X$, that for any
sets $X$ and $Y$ such that $X \subset Y$ we have $|X| < |Y|$, and that if there
exists an injective function $f : X -> Y$ then $|X| = |f(X)|$?

We've already given away that this isn't possible.  The question becomes, how do
we prove it?  It isn't enough to just try a few definitions of `size' and see
that they don't work, as there'll always be a few that we haven't yet attempted.
We need a way of showing that whatever definition we come up with, we'll be able
to show it doesn't work.

To do that, we'll assume that we were given some definition of `size' that
satisfies the above requirements, but with no other assumptions.  You can
imagine it as being given the definition by a friend, and now you have to show
why it's wrong.  If the definition allows us to derive something that isn't
true then there's certainly something wrong with it, so that's what we'll try to
do.

Assume that for every set $X$, we defined $|X|$.  Define $f : \mathbb{Z} \to
\mathbb{Z}$ by $f(x) = 2x$.  We required that $|\mathbb{Z}| = |f(\mathbb{Z})|$.
But because $1 \not \in f(\mathbb{Z})$, $f(\mathbb{Z}) \subset \mathbb{Z}$.
This implies $|f(\mathbb{Z})| < |\mathbb{Z}|$, and so we conclude $|\mathbb{Z}|
= |f(\mathbb{Z})| < |\mathbb{Z}|$.  Surely, though, a set can't be smaller than
itself, so we've just derived a falsehood, and the definition of `size' that we
started with can't work.

If this proof doesn't feel quite right to you, don't worry: we'll look at
arguments like this in-depth in the next chapter.  For now, the main result is
that we are too strict in how we want to define size.  We need to relax one of
the requirements, and in the next section we'll see that if we remove the
requirement that $X \subset Y$ implies $|X| < |Y|$, we can find a good
definition.

\sss{Exercises}

In these exercises we'll explore some more operations for constructing sets.
The three most important ones are {\it union}, {\it intersection}, and {\it
difference}.

The union of $X$ and $Y$ is written as $X \cup Y$ and contains every element of
$X$ and every element of $Y$.  We can write this using set comprehension
notation:
\[
  X \cup Y = \{ x \, | \, x \in X \text{ or } x \in Y \}
\]

\begin{enumerate}
  \item Write out the following sets:
    \begin{enumerate}
      \item $\{0, 1\} \cup \{2, 3\}$
      \item $\{0, 1\} \cup \{1, 2\}$
      \item $\{0, 1\} \cup \{0, 1\}$
      \item $\{0, 1\} \cup \emptyset$
    \end{enumerate}
  \item We say that a set $A$ is the identity of $\cup$ if for every set $X$ we
    have $X \cup A = X = A \cup X$.  Does $\cup$ have an identity?
  \item Suppose $X$ and $Y$ are sets.  Convince yourself that $X \cup Y = Y \cup
    X$.  This property of $\cup$ is called {\it commutativity}.
  \item Suppose $X$, $Y$, and $Z$ are sets.  Convince yourself that $(X \cup Y) = Y \cup
    X$.  This property is called {\it associativity}.
\end{enumerate}

The intersection of $X$ and $Y$ is written as $X \cap Y$ and contains all
elements that are both in $X$ and $Y$.

\begin{enumerate}
  \item Do the first exercise on unions again, but replace $\cup$ with $\cap$.
  \item Let $X$ and $Y$ be sets.  Write $X \cap Y$ as a set comprehension.
  \item Does $\cap$ have an identity?
  \item Is $\cap$ commutative and associative, just like $\cup$ is?
\end{enumerate}

The difference between $X$ and $Y$ is written as $X-Y$ (or, sometimes, $X \ Y$)
and contains all elements that are in $X$ but are not in $Y$.

\begin{enumerate}
  \item Do the first exercise on unions a third time, but now taking the
    difference.
  \item Does set difference have an identity?
  \item Is set difference commutative?  What about associative?
\end{enumerate}

\s{Cardinalities}

% Recall that injective functions allow us to conclude |X| <= |Y|.  Introduce
% surjections and bijections and argue that if f : X -> Y bijective, then f
% injective, thus |X| = |f(X)|, but also f(X) = Y, thus |X| = |Y|.  State,
% without proof, the Cantor-Shroder-Bernstein theorem, and define properly what
% |X| <= |Y| and |X| = |Y| mean.

% Exercises: show |N| = |Z| = |Q+| (introduce products).  Show |Q+| = |Q|.

\s{The Power Set}

% Define the power set.  Prove that the power set of X is strictly greater than
% X.

% Exercises: Continuum hypothesis.

\s{The Category of Sets}

% Point out the structure of "objects and arrows between objects".  Mention that
% this is called a category, and point out some properties of functions
% (composition (+associativity), existence of identities).  Express bijectivity
% as a categorical notion.

% Exercises: Express injectivity and surjectivity as categorical notions.

\s{Special kinds of sets}
\ss{Magmas}
\ss{Semigroups}
\ss{Monoids}